# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170927142032) do

  create_table "complaints", force: :cascade do |t|
    t.string "vehicle"
    t.string "reason"
    t.string "line"
    t.string "race"
    t.string "schooling"
    t.string "sex"
    t.string "institution"
    t.string "relationship"
    t.datetime "occurrence_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "race"
    t.string "schooling"
    t.string "sex"
    t.string "institution"
    t.integer "active", default: 1
    t.integer "access_level", default: 0
    t.string "password_hash"
    t.string "password_salt"
    t.string "api_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
