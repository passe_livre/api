class CreateComplaints < ActiveRecord::Migration[5.1]
  def change
    create_table :complaints do |t|
      t.string :vehicle
      t.string :reason
      t.string :line
      t.string :race
      t.string :schooling
      t.string :sex
      t.string :institution
      t.string :relationship
      t.datetime :occurrence_date

      t.timestamps
    end
  end
end
