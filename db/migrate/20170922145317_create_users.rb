class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :race
      t.string :schooling
      t.string :sex
      t.string :institution
      t.integer :active, default: 1 # 1 -> true, 0 -> false
      t.integer :access_level, default: 0 # 0 -> User, 1 -> Administrator
      t.string :password_hash
      t.string :password_salt
      t.string :api_key

      t.timestamps
    end
  end
end
