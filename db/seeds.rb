# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(email: 'viny-pinheiro@hotmail.com',
            race: 'pardo',
            schooling: 'Ensino Superior',
            institution: 'UNB',
            sex: 'Masculino',
            password: '123456',
            password_confirmation: '123456'
           )
User.create(email: 'admin@admin.com',
            race: 'pardo',
            schooling: 'Ensino Superior',
            institution: 'UNB',
            sex: 'Masculino',
            password: '123456',
            password_confirmation: '123456'
           )
