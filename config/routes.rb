Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # Complaint's API
  get 'complaints/' => 'complaints#index'
  get 'complaints/show/:id' => 'complaints#show'
  post 'complaints/create' => 'complaints#create'

  # User's API
  get 'users/' => 'users#index'
  post 'users/login' => 'users#access_token'
  post 'users/create' => 'users#create'
  patch 'users/update' => 'users#update'
  delete 'users/destroy' => 'users#destroy'
  get 'users/my_profile' => 'users#my_profile'
  post 'users/disable' => 'users#disable_user_by_admin'
end
