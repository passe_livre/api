class Complaint < ApplicationRecord
  # Exception messages
  VEHICLE_CANT_BLANCK         = 'Preencha o veículo'.freeze
  REASON_CANT_BLANCK          = 'Preencha o motivo'.freeze
  LINE_CANT_BLANCK            = 'Preencha a linha'.freeze
  RACE_CANT_BLANCK            = 'Preencha a raça/cor'.freeze
  SCHOOLING_CANT_BLANCK       = 'Preencha a escolaridade'.freeze
  SEX_CANT_BLANCK             = 'Preencha o sexo'.freeze
  INSTITUTION_CANT_BLANCK     = 'Preencha a instituição de ensino'.freeze
  RELATIONSHIP_CANT_BLANCK    = 'Preencha a relação'.freeze
  OCCURRENCE_DATE_CANT_BLANCK = 'Preencha a data de ocorrencia'.freeze

  # Validates
  validates :vehicle, presence: { message: VEHICLE_CANT_BLANCK }
  validates :reason, presence: { message: REASON_CANT_BLANCK }
  validates :line, presence: { message: LINE_CANT_BLANCK }
  validates :race, presence: { message: RACE_CANT_BLANCK }
  validates :schooling, presence: { message: SCHOOLING_CANT_BLANCK }
  validates :sex, presence: { message: SEX_CANT_BLANCK }
  validates :institution, presence: { message: INSTITUTION_CANT_BLANCK }
  validates :occurrence_date, presence: { message: OCCURRENCE_DATE_CANT_BLANCK }
end
