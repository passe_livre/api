class User < ApplicationRecord
  attr_accessor :password
  before_save :encrypt_password
  before_save do |doc|
    doc.api_key = doc.generate_api_key
  end

  # Exception messages
  DIFERENT_PASSWORDS        = 'Senhas Diferem'.freeze
  PASSWORD_CANT_BLANCK      = 'Preencha a Senha'.freeze
  PASSWORD_ISNT_LESS_THAN_6 = 'Senha deve conter no mínimo 6 caracteres'.freeze
  EMAIL_CANT_BLANCK         = 'Preencha o Email'.freeze
  EMAIL_CAN_EXISTS          = 'Email já cadastrado'.freeze
  RACE_CANT_BLANCK          = 'Preencha a raça'.freeze
  SCHOOLING_CANT_BLANCK     = 'Preencha a escolaridade'.freeze
  SEX_CANT_BLANCK           = 'Preencha o sexo'.freeze
  INSTITUTION_CANT_BLANCK   = 'Preencha a instituição'.freeze

  # Validates
  validates :password, confirmation: { message: DIFERENT_PASSWORDS }
  validates :password, presence: { message: PASSWORD_CANT_BLANCK }, on: :create
  validates :password, length: { minimum: 6,
                                 too_short: PASSWORD_ISNT_LESS_THAN_6 },
                       if: :password
  validates :email, presence: { message: EMAIL_CANT_BLANCK }
  validates :email, uniqueness: { message: EMAIL_CAN_EXISTS }
  validates :race, presence: { message: RACE_CANT_BLANCK }
  validates :schooling, presence: { message: SCHOOLING_CANT_BLANCK }
  validates :sex, presence: { message: SEX_CANT_BLANCK }
  validates :institution, presence: { message: INSTITUTION_CANT_BLANCK }

  # Model method to check user authenticate
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user &&
       user.password_hash == BCrypt::Engine.hash_secret(password,
                                                        user.password_salt)
      user
    end
  end

  def self.get_user_by_token(token)
    User.find_by(api_key: token)
  end

  def encrypt_password
    return unless password.present?
    self.password_salt = BCrypt::Engine.generate_salt
    self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
  end

  def generate_api_key
    loop do
      token = SecureRandom.base64.tr('+/=', 'Sbk')
      break token if User.find_by(api_key: token).nil?
    end
  end
end
