class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods

  private

  def authenticate
    user = nil
    authenticate_or_request_with_http_token do |token, _options|
      user = User.get_user_by_token(token)
    end
    user
  end

  def admin_authorize?
    user = authenticate
    # Verify if access level is administrator
    head(:unauthorized) unless user.access_level == 1
  end

  def user_active?
    user = authenticate
    # Verify if user is active
    error_message = 'Usuário sem autorização'

    return unless user.active.zero?

    render json: { failed: 1, errors: error_message }
    nil
  end
end
