class ComplaintsController < ApplicationController
  def index
    render json: Complaint.all
  end

  def show
    if Complaint.exists?(params[:id])
      render json: { failed: 0, data: Complaint.find(params[:id]) }
    else
      render json: { failed: 1, errors: 'Reclamação não encontrada' }
    end
  end

  def create
    user = authenticate
    return if user.nil?
    complaint = mount_complaints(user)
    if complaint.save
      render json: { failed: 0 }
    else
      render json: { failed: 1, errors: complaint.errors }
    end
  end

  private

  def all_params_of_complaints
    params.permit(:vehicle,
                  :reason,
                  :line,
                  :race,
                  :schooling,
                  :sex,
                  :institution,
                  :relationship,
                  :occurrence_date)
  end

  def mount_complaints(user)
    complaint = Complaint.new(all_params_of_complaints)
    if params[:relationship].nil?
      # byebug
      complaint.race = user.race
      complaint.schooling = user.schooling
      complaint.sex = user.sex
      complaint.institution = user.institution
    end
    complaint
  end
end
