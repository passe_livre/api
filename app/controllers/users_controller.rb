class UsersController < ApplicationController
  before_action :authenticate, :user_active?, except: %i[access_token
                                                         create index]
  before_action :admin_authorize?, only: :disable_user_by_admin

  def index
    render json: { failed: 0, data: User.all.select('id',
                                                    'race',
                                                    'schooling',
                                                    'sex',
                                                    'access_level',
                                                    'active') }
  end

  def access_token
    user = User.authenticate(params['email'], params['password'])
    if user.nil?
      render json: { failed: 1, error: 'Usuário não encontrado' }
    elsif user.active.zero?
      render json: { failed: 1, error: 'Usuário sem autorização' }
    else
      render json: { failed: 0, token: user.api_key }
    end
  end

  def create
    user = User.new(all_params_of_user)
    if user.save
      render json: { failed: 0 }
    else
      render json: { failed: 1, errors: user.errors }
    end
  end

  def my_profile
    user = authenticate
    render json: { failed: 0, data: user }
  end

  def disable_user_by_admin
    user = User.find(params[:id])
    user.update_attribute('active', 0)
    render json: { failed: 0, token: user.reload.api_key }
  end

  def update
    user = authenticate
    if user.update(all_params_of_user)
      render json: { failed: 0, token: user.reload.api_key }
    else
      render json: { failed: 1, data: user.errors }
    end
  end

  def destroy
    user = authenticate
    user.destroy
    render json: { failed: 0 }
  end

  private

  def all_params_of_user
    params.permit(:email,
                  :race,
                  :schooling,
                  :sex,
                  :institution,
                  :password,
                  :password_confirmation)
  end
end
