require 'rails_helper'

RSpec.describe Complaint, type: :model do
  it 'create valid complaint' do
    complaint = Complaint.create(vehicle: 'ABC1234',
                                 reason: 'Meu cartão explodiu',
                                 line: '150.0',
                                 race: 'Pardo',
                                 schooling: 'Ensino Médio',
                                 sex: 'Masculino',
                                 institution: 'UNB',
                                 relationship: 'família',
                                 occurrence_date: '2017-12-25')
    expect(complaint).to be_valid
  end

  it 'create without vehicle' do
    complaint = Complaint.create(vehicle: '',
                                 reason: 'Meu cartão explodiu',
                                 line: '150.0',
                                 race: 'Pardo',
                                 schooling: 'Ensino Médio',
                                 sex: 'Masculino',
                                 institution: 'UNB',
                                 relationship: 'família',
                                 occurrence_date: '2017-12-25')
    expect(complaint).to_not be_valid
  end

  it 'create without reason' do
    complaint = Complaint.create(vehicle: 'ABC1234',
                                 reason: '',
                                 line: '150.0',
                                 race: 'Pardo',
                                 schooling: 'Ensino Médio',
                                 sex: 'Masculino',
                                 institution: 'UNB',
                                 relationship: 'família',
                                 occurrence_date: '2017-12-25')
    expect(complaint).to_not be_valid
  end

  it 'create without line' do
    complaint = Complaint.create(vehicle: 'ABC1234',
                                 reason: 'Meu cartão explodiu',
                                 line: '',
                                 race: 'Pardo',
                                 schooling: 'Ensino Médio',
                                 sex: 'Masculino',
                                 institution: 'UNB',
                                 relationship: 'família',
                                 occurrence_date: '2017-12-25')
    expect(complaint).to_not be_valid
  end

  it 'create without race' do
    complaint = Complaint.create(vehicle: 'ABC1234',
                                 reason: 'Meu cartão explodiu',
                                 line: '150.0',
                                 race: '',
                                 schooling: 'Ensino Médio',
                                 sex: 'Masculino',
                                 institution: 'UNB',
                                 relationship: 'família',
                                 occurrence_date: '2017-12-25')
    expect(complaint).to_not be_valid
  end

  it 'create without schooling' do
    complaint = Complaint.create(vehicle: 'ABC1234',
                                 reason: 'Meu cartão explodiu',
                                 line: '150.0',
                                 race: 'Pardo',
                                 schooling: '',
                                 sex: 'Masculino',
                                 institution: 'UNB',
                                 relationship: 'família',
                                 occurrence_date: '2017-12-25')
    expect(complaint).to_not be_valid
  end

  it 'create without sex' do
    complaint = Complaint.create(vehicle: 'ABC1234',
                                 reason: 'Meu cartão explodiu',
                                 line: '150.0',
                                 race: 'Pardo',
                                 schooling: 'Ensino Médio',
                                 sex: '',
                                 institution: 'UNB',
                                 relationship: 'família',
                                 occurrence_date: '2017-12-25')
    expect(complaint).to_not be_valid
  end

  it 'create without institution' do
    complaint = Complaint.create(vehicle: 'ABC1234',
                                 reason: 'Meu cartão explodiu',
                                 line: '150.0',
                                 race: 'Pardo',
                                 schooling: 'Ensino Médio',
                                 sex: 'Masculino',
                                 institution: '',
                                 relationship: 'família',
                                 occurrence_date: '2017-12-25')
    expect(complaint).to_not be_valid
  end

  it 'create without relationship' do
    complaint = Complaint.create(vehicle: 'ABC1234',
                                 reason: 'Meu cartão explodiu',
                                 line: '150.0',
                                 race: 'Pardo',
                                 schooling: 'Ensino Médio',
                                 sex: 'Masculino',
                                 institution: 'UNB',
                                 relationship: '',
                                 occurrence_date: '2017-12-25')
    expect(complaint).to be_valid
  end

  it 'create without occurrence_date' do
    complaint = Complaint.create(vehicle: 'ABC1234',
                                 reason: 'Meu cartão explodiu',
                                 line: '150.0',
                                 race: 'Pardo',
                                 schooling: 'Ensino Médio',
                                 sex: 'Masculino',
                                 institution: 'UNB',
                                 relationship: 'família',
                                 occurrence_date: '')
    expect(complaint).to_not be_valid
  end
end
