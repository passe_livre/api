require 'rails_helper'

RSpec.describe ComplaintsController, type: :controller do
  describe 'rendering' do
    it 'index page' do
      get :index
      expect(response).to have_http_status(200)
    end

    it 'should create without api token' do
      post :create, params: { vehicle: '1112',
                              reason: 'Passe livre não passou',
                              line: 'TR21',
                              race: 'negro',
                              schooling: 'Ensino Superior',
                              sex: 'M',
                              institution: 'UnB',
                              occurrence_date: '2017-02-14' }

      expect(response).to have_http_status(401)
    end
  end

  describe 'Show complaint' do
    before(:each) do
      @complaint = Complaint.create(vehicle: '1112',
                                    reason: 'Passe livre não passou',
                                    line: 'TR21',
                                    race: 'negro',
                                    schooling: 'Ensino Superior',
                                    sex: 'M',
                                    institution: 'UnB',
                                    occurrence_date: '2017-02-14'
                                    )
      @complaint.reload
    end

    it 'Should get complaint' do
      get :show, params: {id: @complaint.id}

      expect(response).to have_http_status(200)
      expect(response.body).to include_json(
        failed: 0
      )
    end

    it 'Should get complaint with not found id' do
      get :show, params: {id: 75839}

      expect(response).to have_http_status(200)
      expect(response.body).to include_json(
        failed: 1
      )
    end
  end

  describe 'New complaint' do
    before(:each) do
      @user = User.create(email: 'viny-pinheiro@hotmail.com',
                          race: 'pardo',
                          institution: 'UNB',
                          schooling: 'Ensino Superior',
                          sex: 'Masculino',
                          password: '123456',
                          password_confirmation: '123456'
                 )
      @user.reload
    end

    it 'should create without relationship' do
      request.headers['Authorization'] = "Token token=#{@user.api_key}"
      post :create, params: { vehicle: '1112',
                              reason: 'Passe livre não passou',
                              line: 'TR21',
                              occurrence_date: '2017-02-14' }

      expect(response).to have_http_status(200)
      expect(response.body).to include_json(
        failed: 0
      )
    end

    it 'should create with relationship' do
      request.headers['Authorization'] = "Token token=#{@user.api_key}"
      post :create, params: { vehicle: '1112',
                              reason: 'Passe livre não passou',
                              line: 'TR21',
                              race: 'negro',
                              schooling: 'Ensino Superior',
                              relationship: 'pai',
                              sex: 'M',
                              institution: 'UnB',
                              occurrence_date: '2017-02-14' }

      expect(response).to have_http_status(200)
      expect(response.body).to include_json(
        failed: 0
      )
    end

    it 'should create without relationship and invalid reason' do
      request.headers['Authorization'] = "Token token=#{@user.api_key}"
      post :create, params: { vehicle: '1112',
                              line: 'TR21',
                              occurrence_date: '2017-02-14' }

      expect(response).to have_http_status(200)
      expect(response.body).to include_json(
        failed: 1
      )
    end

    it 'should create with relationship and invalid reason' do
      request.headers['Authorization'] = "Token token=#{@user.api_key}"
      post :create, params: { vehicle: '1112',
                              line: 'TR21',
                              race: 'negro',
                              schooling: 'Ensino Superior',
                              relationship: 'pai',
                              sex: 'M',
                              institution: 'UnB',
                              occurrence_date: '2017-02-14' }

      expect(response).to have_http_status(200)
      expect(response.body).to include_json(
        failed: 1
      )
    end
  end
end
