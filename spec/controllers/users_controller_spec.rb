require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe 'CRUD of logged user' do
    before(:each) do
      @user = User.create(email: 'viny-pinheiro@hotmail.com',
                          race: 'pardo',
                          institution: 'UNB',
                          schooling: 'Ensino Superior',
                          sex: 'Masculino',
                          password: '123456',
                          password_confirmation: '123456'
                 )
      @user.reload
    end

    it 'Should get token passing valid user password' do
      post :access_token, params: { email: @user.email,
                                    password: @user.password }

      expect(response).to have_http_status(200)
      expect(response.body).to include_json(
        token: @user.api_key
      )
    end

    it 'Should not login with wrong email' do
      post :access_token, params: { email: 'errado@errado.com',
                                    password: @user.password }

      expect(response).to have_http_status(200)
      expect(response.body).to include_json(
        failed: 1,
        error: 'Usuário não encontrado'
      )
    end

    it 'Desactivated user should not login' do
      desactive_user = User.create(email: 'disactivate@disactivate.com',
                  race: 'pardo',
                  institution: 'UNB',
                  schooling: 'Ensino Superior',
                  sex: 'Masculino',
                  password: '123456',
                  password_confirmation: '123456',
                  active: 0
                  )
      post :access_token, params: { email: desactive_user.email,
                                    password: desactive_user.password }

      expect(response).to have_http_status(200)
      expect(response.body).to include_json(
        failed: 1,
        error: 'Usuário sem autorização'
      )
    end
  end

  describe 'Get Users or user' do
      before(:each) do
        @user = User.create(email: 'viny-pinheiro@hotmail.com',
                            race: 'pardo',
                            institution: 'UNB',
                            schooling: 'Ensino Superior',
                            sex: 'Masculino',
                            password: '123456',
                            password_confirmation: '123456'
                   )
        @user.reload
      end

    it 'Should show all users' do
      get :index
      parsed_response = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(parsed_response['failed']).to eq(0)
      expect(parsed_response['data'].size).to eq(User.count)
    end

    it 'Should show user' do
      request.headers['Authorization'] = "Token token=#{@user.api_key}"
      get :my_profile

      parsed_response = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(parsed_response['failed']).to eq(0)
    end

    it 'Should show user with wrong token' do
      request.headers['Authorization'] = "Token token=err#{@user.api_key}"
      get :my_profile

      expect(response).to have_http_status(401)
    end
  end

  describe 'Admin privileges' do
    before(:each) do
      @user_admin = User.create(email: 'viny-pinheiro@hotmail.com',
                                race: 'pardo',
                                institution: 'UNB',
                                schooling: 'Ensino Superior',
                                sex: 'Masculino',
                                password: '123456',
                                password_confirmation: '123456',
                                access_level: 1
                               )
      @user_admin.reload

      @user = User.create(email: 'viny@hotmail.com',
                          race: 'pardo',
                          institution: 'UNB',
                          schooling: 'Ensino Superior',
                          sex: 'Masculino',
                          password: '123456',
                          password_confirmation: '123456'
                         )
      @user.reload
    end

    it 'Should desactivate user' do
      request.headers['Authorization'] = "Token token=#{@user_admin.api_key}"
      post :disable_user_by_admin, params: {id: @user.id}
      @user.reload
      expect(@user.active).to eq(0)
    end

    it 'Should desactivate user without admin acount' do
      request.headers['Authorization'] = "Token token=#{@user.api_key}"
      post :disable_user_by_admin, params: {id: @user.id}
      expect(response).to have_http_status(401)
    end
  end

  describe 'Create' do
    it 'New account' do
      post :create, params: { email: 'viny@hotmail.com',
                              race: 'pardo',
                              institution: 'UNB',
                              schooling: 'Ensino Superior',
                              sex: 'Masculino',
                              password: '123456',
                              password_confirmation: '123456' }

      expect(response.body).to include_json(
        failed: 0
      )
    end

    it 'New account with invalid data' do
      post :create, params: { email: 'viny@hotmail.com',
                              race: 'pardo',
                              institution: 'UNB',
                              schooling: 'Ensino Superior',
                              sex: 'Masculino',
                              password: '1234526',
                              password_confirmation: '123456' }

      expect(response.body).to include_json(
        failed: 1
      )
    end
  end

  describe 'Update' do
    before(:each) do
      @user = User.create(email: 'viny@hotmail.com',
                          race: 'pardo',
                          institution: 'UNB',
                          schooling: 'Ensino Superior',
                          sex: 'Masculino',
                          password: '123456',
                          password_confirmation: '123456'
                         )
      @user.reload
    end

    it 'Should update account without authorization token' do
      patch :update, params: { schooling: 'Ensino Médio' }
      @user.reload

      expect(response).to have_http_status(401)
    end

    it 'Should update my account' do
      request.headers['Authorization'] = "Token token=#{@user.api_key}"
      patch :update, params: { schooling: 'Ensino Médio' }
      @user.reload

      expect(@user.schooling).to eq('Ensino Médio')
      expect(response.body).to include_json(
        failed: 0
      )
    end


    it 'Should update my account with diferent password' do
      request.headers['Authorization'] = "Token token=#{@user.api_key}"
      patch :update, params: { password: '1234a56',
                              password_confirmation: '123456' }
      @user.reload

      expect(response.body).to include_json(
        failed: 1
      )
    end
  end

  describe 'Destroy account' do
    before(:each) do
      @user = User.create(email: 'viny@hotmail.com',
                          race: 'pardo',
                          institution: 'UNB',
                          schooling: 'Ensino Superior',
                          sex: 'Masculino',
                          password: '123456',
                          password_confirmation: '123456'
                         )
      @user.reload
    end

    it 'should destroy' do
      request.headers['Authorization'] = "Token token=#{@user.api_key}"
      delete :destroy

      expect(response.body).to include_json(
        failed: 0
      )
    end

    it 'should destroy inactive user' do
      @user.update_attribute('active', 0)
      request.headers['Authorization'] = "Token token=#{@user.api_key}"
      delete :destroy

      expect(response.body).to include_json(
        failed: 1
      )
      expect(User.find(@user.id)).to_not be_nil
    end
    it 'should destroy with wrong token' do
      request.headers['Authorization'] = "Token token=asa#{@user.api_key}"
      delete :destroy

      expect(response).to have_http_status(401)
    end
  end
end
